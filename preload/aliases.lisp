
(fset 'string-concat #'string)
(fset 'map #'mapcar)
(fset 'λ #'lambda)
(fset 'eql #'eq)

(fset 'file-namestring #'basename)
(fset 'directory-namestring #'dirname)

;; (fset 'namestring 'normalize-pathname)

;; (fset 'char-code #'char-int)
;; (fset 'char= #'=)
;; (fset 'char/= #'/=)
;; (fset 'char< #'<)
;; (fset 'char> #'>)
;; (fset 'char<= #'<=)
;; (fset 'char>= #'>=)

;;;EOF
